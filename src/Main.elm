module Main exposing (main)

import Animator
import Browser
import Browser.Dom
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Html.Attributes
import Task
import Time


type Page
    = Home
    | Places
    | Trips


type alias Flags =
    ()


type alias Model =
    { page : Animator.Timeline Page
    , pageForMeasuring : Maybe Page
    , profileImageMeasurement : Maybe Measurement
    }


type alias Measurement =
    { old : Browser.Dom.Element
    , new : Browser.Dom.Element
    }


initialModel : Model
initialModel =
    { page = Animator.init Home
    , pageForMeasuring = Nothing
    , profileImageMeasurement = Nothing
    }


main =
    Browser.document
        { init = \() -> ( initialModel, Cmd.none )
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ animator
            |> Animator.toSubscription Tick model
        ]


animator : Animator.Animator Model
animator =
    Animator.animator
        |> Animator.watchingWith .page
            (\newPage model ->
                { model | page = newPage }
            )
            (\_ -> False)


type Msg
    = GoToPage Page
    | ElementsMeasured Page (Result Browser.Dom.Error Measurement)
    | Tick Time.Posix


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GoToPage newPage ->
            ( { model
                | pageForMeasuring = Just newPage
              }
            , Task.map2 Measurement
                (Browser.Dom.getElement "profile-image")
                (Browser.Dom.getElement "new:profile-image")
                |> Task.attempt (ElementsMeasured newPage)
            )

        ElementsMeasured newPage result ->
            ( { model
                | page =
                    model.page
                        |> Animator.go (Animator.millis 900) newPage
                , profileImageMeasurement = Result.toMaybe result
                , pageForMeasuring = Nothing
              }
            , Cmd.none
            )

        Tick newTime ->
            ( Animator.update newTime animator model
            , Cmd.none
            )


view : Model -> Browser.Document Msg
view model =
    { title = "Travel app demo"
    , body =
        [ layout
            [ width fill
            , height fill
            , behindContent <|
                case model.pageForMeasuring of
                    Nothing ->
                        none

                    Just pageForMeasuring ->
                        el
                            [ width fill
                            , htmlAttribute (Html.Attributes.style "visibility" "hidden")
                            ]
                            (viewPage "new:"
                                (Animator.init pageForMeasuring)
                                Nothing
                            )
            ]
            (viewPage "" model.page model.profileImageMeasurement)
        ]
    }


viewPage idPrefix page profileImageMeasurement =
    case Animator.current page of
        Home ->
            viewHome idPrefix page profileImageMeasurement

        Places ->
            viewPlaces idPrefix page profileImageMeasurement

        _ ->
            el
                [ centerX
                , centerY
                ]
                (text (Debug.toString page))


headerImageHeight =
    300


headerImage : Page -> String
headerImage page =
    case page of
        Home ->
            "header1.jpg"

        Places ->
            "header2.jpg"

        Trips ->
            "header3.jpg"


backgroundImage : Animator.Timeline Page -> Element msg
backgroundImage timeline =
    let
        alphaValue =
            Animator.move timeline <|
                \page ->
                    if page == Animator.current timeline then
                        Animator.at 0

                    else
                        Animator.at 1

        previous =
            el
                [ width fill
                , height fill
                , Background.image (headerImage <| Animator.previous timeline)
                , alpha alphaValue
                ]
                none
    in
    el
        [ width fill
        , height (px headerImageHeight)
        , Background.image (headerImage <| Animator.current timeline)
        , inFront previous
        ]
        none


viewHome :
    String
    -> Animator.Timeline Page
    -> Maybe Measurement
    -> Element Msg
viewHome idPrefix timeline profileImageMeasurement =
    let
        navBarHeight =
            50

        viewNavButton ( linkText, page ) =
            Input.button
                [ Font.color (rgb 1 1 1)
                ]
                { onPress = Just (GoToPage page)
                , label = text linkText
                }
    in
    el
        [ behindContent (backgroundImage timeline)
        , width fill
        ]
        (column
            [ width (px 1000)
            , centerX
            ]
            [ row
                [ height (px navBarHeight)
                , width fill
                , Background.color (rgba 0 0 0 0.4)
                ]
                [ row
                    [ alignLeft
                    , spacing 60
                    ]
                    (List.map viewNavButton
                        [ ( "Jill's Home", Home )
                        , ( "Jill's Places", Places )
                        , ( "Jill's Group Trips", Trips )
                        ]
                    )
                , placeholder [ alignRight ] "... menu"
                ]
            , homeHeader idPrefix timeline profileImageMeasurement navBarHeight
            , let
                alphaValue =
                    Animator.move timeline <|
                        \page ->
                            case page of
                                Home ->
                                    Animator.at 1

                                _ ->
                                    Animator.at 0
              in
              el
                [ paddingXY 0 30
                , width fill
                , alpha alphaValue
                ]
                (placeholder
                    [ width fill
                    , height (px 400)
                    , Background.color (rgb 0.0941 0.2941 0.0392)
                    ]
                    "body content"
                )
            ]
        )


homeHeader :
    String
    -> Animator.Timeline Page
    -> Maybe Measurement
    -> Int
    -> Element msg
homeHeader idPrefix timeline profileImageMeasurement navBarHeight =
    row
        [ height (px <| headerImageHeight + 120 - navBarHeight)
        , width fill
        , spacing 30
        ]
        [ column
            [ spacing 15
            ]
            [ row
                [ spacing 20
                , height (px 280)
                ]
                [ viewProfileImage
                    200
                    200
                    [ alignBottom ]
                    idPrefix
                    timeline
                    profileImageMeasurement
                , el
                    [ Background.color (rgb 1 0.27 0)
                    , padding 10
                    , Font.color (rgb 1 1 1)
                    , Border.rounded 5
                    , alignBottom
                    , width (px 150)
                    , Font.center
                    ]
                    (text "Follow")
                ]
            , el [ Font.size 35 ]
                (text "Jill Fernandez")
            ]
        , column
            [ alignRight
            , width fill
            , alignBottom
            ]
            [ placeholder
                [ width fill
                ]
                "empty space with Mail button"
            , row
                [ height (px 120)
                ]
                [ placeholder [] "bio"
                , placeholder [] "follower count"
                , placeholder [] "following count"
                ]
            ]
        ]


viewProfileImage :
    Int
    -> Int
    -> List (Element.Attribute msg)
    -> String
    -> Animator.Timeline Page
    -> Maybe Measurement
    -> Element msg
viewProfileImage w h attrs idPrefix timeline profileImageMeasurement =
    let
        scaleValue =
            Animator.move timeline <|
                \page ->
                    if page == Animator.current timeline then
                        Animator.at 1

                    else
                        case profileImageMeasurement of
                            Nothing ->
                                Animator.at 0

                            Just measurement ->
                                Animator.at
                                    (measurement.old.element.width / toFloat w)

        moveValue =
            Animator.xy timeline <|
                \page ->
                    if page == Animator.current timeline then
                        { x = Animator.at 0
                        , y = Animator.at 0
                        }

                    else
                        case profileImageMeasurement of
                            Just measurement ->
                                { x =
                                    Animator.at
                                        ((measurement.new.element.x - measurement.old.element.x)
                                            + (measurement.new.element.width - measurement.old.element.width)
                                            / 2
                                        )
                                , y =
                                    Animator.at
                                        ((measurement.new.element.y - measurement.old.element.y)
                                            + (measurement.new.element.height - measurement.old.element.height)
                                            / 2
                                        )
                                }

                            Nothing ->
                                { x = Animator.at 0
                                , y = Animator.at 0
                                }
    in
    image
        ([ width (px w)
         , height (px h)
         , Border.rounded 5
         , clip
         , moveUp moveValue.y
         , moveLeft moveValue.x
         , scale scaleValue
         , htmlAttribute (Html.Attributes.id <| idPrefix ++ "profile-image")
         ]
            ++ attrs
        )
        { src = "profile4.jpg"
        , description = "Photo of Jill Fernandez"
        }


viewPlaces :
    String
    -> Animator.Timeline Page
    -> Maybe Measurement
    -> Element Msg
viewPlaces idPrefix timeline profileImageMeasurement =
    let
        navBarHeight =
            50

        viewNavButton ( linkText, page ) =
            Input.button
                [ Font.color (rgb 1 1 1)
                ]
                { onPress = Just (GoToPage page)
                , label = text linkText
                }
    in
    el
        [ behindContent (backgroundImage timeline)
        , width fill
        ]
        (column
            [ width (px 1000)
            , centerX
            ]
            [ row
                [ height (px navBarHeight)
                , width fill
                , Background.color (rgba 0 0 0 0.4)
                ]
                [ row
                    [ alignLeft
                    , spacing 60
                    ]
                    (List.map viewNavButton
                        [ ( "Jill's Home", Home )
                        , ( "Jill's Places", Places )
                        , ( "Jill's Group Trips", Trips )
                        ]
                    )
                , placeholder [ alignRight ] "... menu"
                ]
            , placesHeader idPrefix timeline navBarHeight profileImageMeasurement
            , let
                alphaValue =
                    Animator.move timeline <|
                        \page ->
                            case page of
                                Places ->
                                    Animator.at 1

                                _ ->
                                    Animator.at 0
              in
              el
                [ paddingXY 0 30
                , width fill
                , alpha alphaValue
                ]
                (placeholder
                    [ width fill
                    , height (px 400)
                    , Background.color (rgb 0.1216 0.6275 0.6784)
                    ]
                    "body content"
                )
            ]
        )


placesHeader :
    String
    -> Animator.Timeline Page
    -> Int
    -> Maybe Measurement
    -> Element msg
placesHeader idPrefix timeline navBarHeight profileImageMeasurement =
    row
        [ height (px <| headerImageHeight - navBarHeight)
        , width fill
        , spacing 30
        ]
        [ column
            [ spacing 15
            ]
            [ row
                [ spacing 20
                ]
                [ viewProfileImage
                    150
                    150
                    [ alignBottom ]
                    idPrefix
                    timeline
                    profileImageMeasurement
                ]
            , row
                [ spacing 10
                ]
                [ el
                    [ Background.color (rgb 1 0.27 0)
                    , padding 10
                    , Font.color (rgb 1 1 1)
                    , Border.rounded 5
                    , alignBottom
                    , width (px 150)
                    , Font.center
                    ]
                    (text "Follow")
                , el
                    [ Font.size 26
                    , Font.color (rgb 1 1 1)
                    ]
                    (text "Jill Fernandez")
                ]
            ]
        , column
            [ alignRight
            , width fill
            , alignBottom
            ]
            [ placeholder
                [ width fill
                ]
                "empty space with Plus button"
            , placeholder [ alignRight ] "days remaining"
            ]
        ]


wireframeTheme =
    { bg = rgb 0.9 0.9 0.9
    , frame = rgb 0.5 0.5 0.5
    , text = rgb 0.3 0.3 0.3
    }


placeholder : List (Attribute msg) -> String -> Element msg
placeholder attr name =
    text name
        |> el
            [ Border.rounded 5
            , Border.dotted
            , Border.color wireframeTheme.frame
            , Border.width 2
            , height fill
            , width fill
            , padding 20
            , Background.color wireframeTheme.bg
            , Font.center
            , Font.color wireframeTheme.text
            , alpha 0.4
            ]
        |> el attr
